package com.example.gptest.routes;

import com.example.gptest.dto.CalculatorForm;
import com.example.gptest.dto.CalculatorResponse;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.dataformat.soap.name.ServiceInterfaceStrategy;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tempuri.CalculatorSoap;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Configuration
public class RestInRoute extends RouteBuilder {
    @Override
    public void configure() {
        onException(BeanValidationException.class)
                .to("direct:bad-request")
                .handled(true);

        rest()
                .post("/calc")
                .consumes("application/json")
                .produces("application/json")
                .bindingMode(RestBindingMode.json)
                .type(CalculatorForm.class)
                .outType(CalculatorResponse.class)
                .route()
                .choice().when(this::bodyIsNullOrEmpty).to("direct:bad-request")
                .otherwise()
                .to("bean-validator:/x")
                .process("calculatorRequestMapper")
                .marshal("calculatorSoap")
                .setHeader("Content-Type", constant("application/soap+xml; charset=utf-8"))
                .to("cxf://http://www.dneonline.com/calculator.asmx?serviceClass=org.tempuri.Calculator&dataFormat=RAW")
                .unmarshal("calculatorSoap")
                .process("calculatorResponseMapper");
    }

    private boolean bodyIsNullOrEmpty(Exchange exchange) {
        final Object body = exchange.getMessage().getBody();
        return body == null || String.valueOf(body).isEmpty();
    }

    @Bean
    SoapJaxbDataFormat calculatorSoap() {
        SoapJaxbDataFormat soap = new SoapJaxbDataFormat(
                "org.tempuri",
                new ServiceInterfaceStrategy(CalculatorSoap.class, true)
        );
        soap.setVersion("1.2");

        return soap;
    }
}
