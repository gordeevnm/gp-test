package com.example.gptest.processors;

import com.example.gptest.dto.CalculatorResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Component("calculatorResponseMapper")
public class CalculatorResponseMapper implements Processor {
    @Override
    public void process(Exchange exchange) {
        if (exchange.isFailed()) {
            return;
        }

        final Message message = exchange.getMessage();
        final Object body = message.getBody();

        final Integer result = Arrays.stream(BeanUtils.getPropertyDescriptors(body.getClass()))
                .filter(d -> d.getPropertyType().equals(int.class)) // TODO: 08.03.2021 only int supported
                .findFirst()
                .map(PropertyDescriptor::getReadMethod)
                .map(method -> {
                    try {
                        return (int) method.invoke(body);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new IllegalArgumentException(e.getMessage(), e);
                    }
                })
                .orElseThrow(() -> new IllegalArgumentException("Result value not found in response: " + body));

        message.getHeaders().clear();
        message.setBody(new CalculatorResponse(result));
    }
}
