package com.example.gptest.routes;

import com.example.gptest.dto.CalculatorErrorResponse;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 10.03.2021
 */
@Component
public class BadRequestRoute extends RouteBuilder {
    @Override
    public void configure() {
        from("direct:bad-request")
                .setBody(exchange -> {
                    exchange.setException(null);
                    return new CalculatorErrorResponse();
                })
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(400));
    }
}
