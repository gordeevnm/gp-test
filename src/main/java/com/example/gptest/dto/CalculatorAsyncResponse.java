package com.example.gptest.dto;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CalculatorAsyncResponse {
    private String taskId;
}
