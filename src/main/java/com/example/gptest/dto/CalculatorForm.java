package com.example.gptest.dto;

import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CalculatorForm {
    @NotNull(message = "field must not be null")
    private Operation operation;
    @NotNull(message = "field must not be null")
    private Integer a;
    @NotNull(message = "field must not be null")
    private Integer b;

    public enum Operation {
        ADD, DIVIDE, MULTIPLY, SUBTRACT
    }
}
