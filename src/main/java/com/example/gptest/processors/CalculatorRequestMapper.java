package com.example.gptest.processors;

import com.example.gptest.dto.CalculatorForm;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import org.tempuri.Add;
import org.tempuri.Divide;
import org.tempuri.Multiply;
import org.tempuri.Subtract;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Component("calculatorRequestMapper")
@RequiredArgsConstructor
public class CalculatorRequestMapper implements Processor {
    @Override
    public void process(Exchange exchange) {
        if (exchange.isFailed()) {
            return;
        }

        final Message message = exchange.getMessage();
        final CalculatorForm body = message.getBody(CalculatorForm.class);
        final CalculatorForm.Operation operation = body.getOperation();

        final Object op;

        // FIXME: 08.03.2021
        switch (operation) {
            case ADD:
                final Add add = new Add();
                add.setIntA(body.getA());
                add.setIntB(body.getB());
                op = add;
                break;
            case DIVIDE:
                final Divide divide = new Divide();
                divide.setIntA(body.getA());
                divide.setIntB(body.getB());
                op = divide;
                break;
            case MULTIPLY:
                final Multiply multiply = new Multiply();
                multiply.setIntA(body.getA());
                multiply.setIntB(body.getB());
                op = multiply;
                break;
            case SUBTRACT:
                final Subtract subtract = new Subtract();
                subtract.setIntA(body.getA());
                subtract.setIntB(body.getB());
                op = subtract;
                break;
            default:
                exchange.setException(new UnsupportedOperationException("Unsupported operation " + operation));
                return;
        }
        message.setBody(op);
    }
}
