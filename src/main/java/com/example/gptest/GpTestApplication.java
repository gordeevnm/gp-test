package com.example.gptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpTestApplication.class, args);
    }

}
