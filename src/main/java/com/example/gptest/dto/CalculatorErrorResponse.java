package com.example.gptest.dto;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class CalculatorErrorResponse {
    // FIXME: 08.03.2021 
    private final String message = "bad request";
}
