package com.example.gptest.routes;

import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 08.03.2021
 */
@Configuration
@RequiredArgsConstructor
public class SwaggerRoute extends RouteBuilder {
    private final ServerProperties serverProperties;

    @Override
    public void configure() throws Exception {
        restConfiguration()
                .component("servlet")
                .apiContextPath("/swagger")
                .contextPath("/camel");

        from("servlet://swagger-ui")
                .setHeader(
                        "Location",
                        constant("/webjars/swagger-ui/index.html?url=/camel/swagger")
                )
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(301));
    }
}
