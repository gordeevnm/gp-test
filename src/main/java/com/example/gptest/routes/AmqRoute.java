// package com.example.gptest.routes;
//
// import com.example.gptest.dto.CalculatorAsyncResponse;
// import lombok.RequiredArgsConstructor;
// import org.apache.camel.ExchangePattern;
// import org.apache.camel.Message;
// import org.apache.camel.builder.RouteBuilder;
// import org.springframework.context.annotation.Configuration;
//
// /**
//  * @author Gordeev Nikita
//  * gordeevnm@gmail.com
//  * 08.03.2021
//  */
// @Configuration
// @RequiredArgsConstructor
// public class AmqRoute extends RouteBuilder {
//
//     @Override
//     public void configure() throws Exception {
//         rest()
//                 .get("/test")
//                 .param()
//                 .name("q")
//                 .defaultValue("asd")
//                 .endParam()
//                 .route()
//                 .to(
//                         ExchangePattern.InOnly,
//                         "jms:queue:in" +
//                                 "?useMessageIDAsCorrelationID=true" +
//                                 "&includeSentJMSMessageID=true"
//                 )
//                 .process(exchange -> {
//                     final Message message = exchange.getMessage();
//                     message.setBody(new CalculatorAsyncResponse(String.valueOf(message.getHeader("JMSMessageID"))));
//                 });
//     }
// }
